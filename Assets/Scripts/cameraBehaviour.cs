﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraBehaviour : MonoBehaviour {


    public Transform t1;
    public Transform t2;
    public Camera cam;
    
    public float zoomFactor = 1.5f;
    public float followTimeDelta = 0.8f;
    public float maxDistance = 14f;
    public float minDistance = 5f;

    public void FixedCameraFollowSmooth()
    {
        

        // Midpoint calculation
        Vector3 midpoint = (t1.position + t2.position) / 2f;

        // Distance between objects
        float distance = (t1.position - t2.position).magnitude;

        //sets a max/min distance for camera
        if (distance > maxDistance)
        {
            distance = maxDistance;
        }
        else if (distance < minDistance)
        {
            distance = minDistance;
        }

        // Move camera a certain distance
        Vector3 cameraDestination = midpoint - cam.transform.forward * distance * zoomFactor;

        // Adjust ortho size
        if (cam.orthographic)
        {
            // only this size will matter
            cam.orthographicSize = distance;
        }
        // use slerp to transform the position
        cam.transform.position = Vector3.Slerp(cam.transform.position, cameraDestination, followTimeDelta);

        // Snap when close enough to prevent annoying slerp behavior
        if ((cameraDestination - cam.transform.position).magnitude <= 0.05f)
            cam.transform.position = cameraDestination;
    }
    

    void FixedUpdate () {
        FixedCameraFollowSmooth();
	}
}
