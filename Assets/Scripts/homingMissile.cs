﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class homingMissile : MonoBehaviour {

    public float startSpeed = 2;
    private static float speed;
    public float rotatingSpeed = 200;
    GameObject closest;

    public float currentSpeed;

    public Collider2D playerHit;

    public AudioSource explode;

    public LivesManager lifesystem;

    public levelManager respawn;

    private string target = "Player";

    Rigidbody2D rb;

    GameObject[] gos;

    
	void Start () {
        speed = startSpeed;

        lifesystem = FindObjectOfType<LivesManager>();
        respawn = FindObjectOfType<levelManager>();

        // increases missile speed over time
        StartCoroutine(coIncreaseMissileSpeed());

        // got from http://answers.unity3d.com/questions/1258352/homing-missile-targetting-the-nearest-enemy.html

        rb = GetComponent<Rigidbody2D>();
    }
    
    
    void FixedUpdate () {

        // sets gos to all gameobjects with the tag of target
        gos = GameObject.FindGameObjectsWithTag(target);
        closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;

        //checks the difference in distance between the gos
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }

        //get the difference between the two positions and aim the missile towards the target
        Vector2 aim = (Vector2)transform.position - (Vector2)closest.transform.position;
        aim.Normalize();

        //sets the direction the missile points to
        float value = Vector3.Cross(aim, transform.up).z;

        //makes the missile rotate smoothly to the player, the closer it is the less it rotates
        rb.angularVelocity = rotatingSpeed * value;
        

        //sets the missile velocity to the speed variable
        rb.velocity = transform.up * speed;

        currentSpeed = rb.velocity.magnitude;
	}

   

    public void OnTriggerEnter2D(Collider2D other)
    {
        // if it hits player 1 or 2, call their death functions respectively
        if (other.tag == "Player 1")
        {
            respawn.PlayerRespawn(other.GetComponent<PlayerInput>());
        }
        if (other.tag == "Player 2")
        {
            respawn.PlayerRespawn(other.GetComponent<PlayerInput>());
        }

        //destroy the missile if it collides with anything but another missile
        if (other.tag != "Missile")
        {
            explode.Play();
            //Destroy(this.gameObject, 0.02f);
            gameObject.SetActive(false);
        }
    }

    // homing missile code from https://www.youtube.com/watch?v=jWUz5J-vfGA

    IEnumerator coIncreaseMissileSpeed()
    {
        // every 10 seconds, increase the missile speed by 10%
        while (true)
        {
            yield return new WaitForSeconds(10f);
            speed = speed * 1.1f;
        }
    }

}
