﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuManager : MonoBehaviour {

    private bool tutorialUp = false;

    public Button mainButton;
    public Button tutorialButton;
    public AudioSource menuChange;
    public AudioSource menuClick;

    int index;

    public GameObject tutorial, menu;

    private void Start()
    {
        // make cursor invisible so that it stops changing focus
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // ALL THESE BUTTONS ARE PART OF THE EVENTSYSTEM
    void Update()
    {
        while (menu.activeInHierarchy == true && index == 1)
        {
            mainButton.Select();

            index--;
        }
    }

    public void LoadMenu (int SceneIndex)
    {
        Time.timeScale = 1;

        SceneManager.LoadScene(SceneIndex);
    }

    public void LoadMenu2(int SceneIndex)
    {
        Time.timeScale = 1;

        SceneManager.LoadScene(SceneIndex);
    }

    public void OnHover()
    {
        menuChange.Play();
    }

    public void OnClick()
    {
        menuClick.Play();
    }

    public void TutorialButton()
    {
        // I don't thik we use this anymore, but it's just a failsafe for the tutorial button focus
        if(tutorial.activeInHierarchy == false)
        {
            menu.SetActive(false);
            tutorial.SetActive(true);
            tutorialButton.Select();
        }
        else
        {
            if (Input.anyKeyDown)
            {
                tutorial.SetActive(false);
                menu.SetActive(true);
                index++;
            }
        }
    }
}
