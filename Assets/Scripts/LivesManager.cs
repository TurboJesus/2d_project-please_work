﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesManager : MonoBehaviour
{
    [Header("Game Objects")]
    public GameObject player1;
    public GameObject player2;
    public GameObject GameOver;

    [Header("Player Winner Text")]
    public GameObject player1Wins;
    public GameObject player2Wins;

    [Header("Lives Array")]
    public int p1Lives;
    public int p2Lives;
    public GameObject[] p1life;
    public GameObject[] p2life;

    private Rigidbody2D p1Rb;
    private Rigidbody2D p2Rb;

    [Header("Player Victory Sounds")]
    public AudioSource player1Win, player2Win;

    public AudioSource winSound1;

    public bool gameEnd = false;

    public static LivesManager instance;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            return;
        }

        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        // check if players lives have reached 0 
        // display win things if lives are at 0
        if (p1Lives <= 0 && gameEnd == false)
        {
            winSound1.Play();
            levelManager.instance.intro.Stop();
            levelManager.instance.loop.Stop();
            player1.SetActive(false);
            player2Wins.SetActive(true);
            player1Wins.SetActive(false);
            GameOver.SetActive(true);

            gameEnd = true;

            Time.timeScale = 0;
        }

        if (p2Lives <= 0 && gameEnd == false)
        {
            winSound1.Play();
            levelManager.instance.intro.Stop();
            levelManager.instance.loop.Stop();
            player2.SetActive(false);
            player1Wins.SetActive(true);
            player2Wins.SetActive(false);
            GameOver.SetActive(true);

            gameEnd = true;

            Time.timeScale = 0;
        }
    }

    public void P1Damage()
    {
        // lower life counter and set the life icons
        p1Lives -= 1;

        for (int i = 0; i < p1life.Length; i++)
        {
            if (p1Lives > i)
            {
                p1life[i].SetActive(true);
            }
            else
            {
                p1life[i].SetActive(false);
            }
        }
    }

    public void P2Damage()
    {
        // lower life counter and set the life icons
        p2Lives -= 1;

        for (int i = 0; i < p2life.Length; i++)
        {

            if (p2Lives > i)
            {
                p2life[i].SetActive(true);
            }
            else
            {
                p2life[i].SetActive(false);
            }
        }
    }
}
