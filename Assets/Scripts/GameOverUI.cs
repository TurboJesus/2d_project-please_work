﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class GameOverUI : MonoBehaviour {

    public GameObject pause;
    public GameObject GO;

    public Button selected;
    public int level;

    int index = 1;
    
    public void Continue()
    {
        
        pause.SetActive(false);
        
        Time.timeScale = 1;

    }

    public void Restart()
    {
        Time.timeScale = 1;
        
        if (level == 1)
            SceneManager.LoadScene("2Dproject prototype");
        else
            SceneManager.LoadScene("LV2");
    }

    public void MainMenu()
    {

        SceneManager.LoadScene("Main Menu");
    }

    public void Quit()
    {

        Application.Quit();
    }
    
    void Update () {
        
        // checks if game over screen is on top of hierarchy
        while (GO.activeInHierarchy == true && index == 1)
        {
            selected.Select();

            index++;
        }
    }
}
