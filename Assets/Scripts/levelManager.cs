﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class levelManager : Singleton<levelManager> {

    [Header("Player Checkpoints")]
    public GameObject checkpoint1;
    public GameObject checkpoint2;
    public GameObject level1;
    public GameObject level2;

    [Header("UI")]
    public GameObject pause;
    public GameObject counter;
    public GameObject three;
    public GameObject two;
    public GameObject one;
    public GameObject go;

    [Header("Animator")]
    Animator anim;

    [Header("Audio")]
    public AudioSource intro;
    public AudioSource loop;
    private bool startedLoop;
    public AudioSource pauseSound;
    public AudioSource wallChange;
    public AudioSource countdownSound;
    public AudioSource goSound;

    [Header("Missiles")]
    public GameObject[] missile;
    public GameObject[] missileAlt;

    [Header("Missile Spawners")]
    public GameObject[] missileSpawn;

    
    private float respawnDelay = 1.5f;

    public Button selected1;
    public LivesManager lifesystem;
    public bool currentLevel;
    public bool isPaused = false;

    // Use this for initialization
    void Start () {

        lifesystem = FindObjectOfType<LivesManager>();

        intro.Play();

        StartCoroutine(coCounter());
        StopCoroutine(coCounter());

        StartCoroutine(coMissileRespawns());
        StartCoroutine(coSwapLevel());
    }

    private void FixedUpdate()
    {
        // makes the music work
        if(!intro.isPlaying && !startedLoop)
        {
            loop.Play();
            //Debug.Log("done playing");

            startedLoop = true;
        }

        Pause();
    }

    public void Pause()
    {
        // opens the pause menu and accesses the pause menu eventsystem
        if(Input.GetKeyDown("joystick button 7"))
        {
            pause.SetActive(true);
            pauseSound.Play();
            selected1.Select();

            //isPaused = true;

            Time.timeScale = 0;
        }
    }

    public void PlayerRespawn(PlayerInput pl)
    {
        StartCoroutine(coRespawnDelay(pl));
    }


    
    private void LevelSwap()
    {
        // every 10 seconds it changes which map is active
        if(level1.activeInHierarchy == true)
        {
            wallChange.Play();

            level1.SetActive(false);
            level2.SetActive(true);

        }
        else if(level2.activeInHierarchy == true)
        {
            wallChange.Play();

            level2.SetActive(false);
            level1.SetActive(true);
        }

    }


    public IEnumerator coRespawnDelay(PlayerInput pl)
    {
        // if the player is invincible, don't run the death script
        if (pl.invincible == false)
        {
            // sets the player inactive
            Debug.Log("start waiting");
            pl.gameObject.SetActive(false);

            // runs the appropriate players death function
            if (pl.gameObject.tag == "Player 1")
            {
                lifesystem.P1Damage();
                pl.transform.position = checkpoint1.transform.position;

            }
            else if (pl.gameObject.tag == "Player 2")
            {
                lifesystem.P2Damage();
                pl.transform.position = checkpoint2.transform.position;
            }

            // wait for the appropriate amount of time then normalize and respawn the player
            yield return new WaitForSeconds(respawnDelay);
            pl.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            pl.gameObject.SetActive(true);

            // respawn invincibility
            pl.invincible = true;
            
            StartCoroutine(coInvincible(pl));

            // set all the things back to normal
            Debug.Log("done waiting");
            pl.isDashing = false;
            pl.isCharging = false;
            pl.staggered = false;
            pl.chargePower = 0;
            pl.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }

    IEnumerator coInvincible(PlayerInput pl)
    {
        // wait 2 seconds while the player is invincible, then set them at not invincible
        while (pl.invincible == true)
        {
            yield return new WaitForSeconds(2);
            pl.invincible = false;
        }
         StopCoroutine(coInvincible(pl));
    }

    IEnumerator coMissileRespawns()
    {
        while (true)
        {
            // spawn missiles every 10 seconds
            Instantiate(missile[0]);
            missile[0].transform.position = missileSpawn[0].transform.position;
            missile[0].SetActive(true);

            Instantiate(missile[1]);
            missile[1].transform.position = missileSpawn[1].transform.position;
            missile[1].SetActive(true);

            Instantiate(missile[2]);
            missile[2].transform.position = missileSpawn[2].transform.position;
            missile[2].SetActive(true);

            Instantiate(missile[3]);
            missile[3].transform.position = missileSpawn[3].transform.position;
            missile[3].SetActive(true);

            yield return new WaitForSeconds(10f);
            
        }
    }
    

    IEnumerator coSwapLevel()
    {
        // do the level swap every 10 seconds
        while (currentLevel == true)
        {
            yield return new WaitForSeconds(10);
            LevelSwap();
        }
    }

    IEnumerator coCounter()
    {
        // displays a countdown number and sound every second
        // only shown at the start of every match
        Time.timeScale = 0;
        counter.SetActive(true);
        three.SetActive(true);
        countdownSound.Play();
        yield return new WaitForSecondsRealtime(1);

        three.SetActive(false);
        two.SetActive(true);
        countdownSound.Play();
        yield return new WaitForSecondsRealtime(1);
        
        two.SetActive(false);
        one.SetActive(true);
        countdownSound.Play();
        yield return new WaitForSecondsRealtime(1);

        one.SetActive(false);
        go.SetActive(true);
        goSound.Play();
        yield return new WaitForSecondsRealtime(1);

        go.SetActive(false);
        counter.SetActive(false);
        Time.timeScale = 1;
    }

    public override void SingletonAwake()
    {
    }
}
