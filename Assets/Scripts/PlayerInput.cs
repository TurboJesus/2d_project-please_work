﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInput : MonoBehaviour {


    [Header("Movement")]
    public float moveSpeed;
    public float chargingMoveSpeed;
    public float weakDashPower;
    public float strongDashPower;
    public float maxSpeed = 50f;

    [Header("Game Object References")]
    public PlayerInput otherPlayer;
    public GameObject GameOver;
    public GameObject spawner;

    [Header("Audio")]
    public AudioSource Dash;
    public AudioSource Bounce;
    public AudioSource PowerDown;
    public AudioSource PowerUp;
    public AudioSource playerHit;

    [Header("Controls")]
    public string horizontalAxis = "Horizontal";
    public string verticalAxis = "Vertical";
    public string horizontalRight = "Right_Horizontal";
    public string verticalRight = "Right_Vertical";
    public string triggerRight = "Right_Trigger";

    public float deadZone = 0.1f;

    private float myCurrentSpeed = 0;
    public float chargePower = 0;
    private float minDashingSpeed;
    private Vector3 lastPos;
    internal bool staggered = false;
    internal bool isDashing = false;
    internal bool isCharging = false;
    internal bool invincible = false;

    [Header("Energy and Charging")]
    public int maxEnergy = 150;
    public int quickDashCost = 50;
    public int playerNumber = 1;
    private int chargeDashCost = 50;
    private int Energy1;
    private int Energy2;
    private int energy
    {
        get
        {
            if (playerNumber == 1)
            {
                return Energy1;
            }
            else
            {
                return Energy2;
            }
        }
        set
        {
            if (playerNumber == 1)
            {
                if (value < 0)
                {
                    // energy stagger is in the get-set for convenience
                    Energy1 = 0;
                    StartCoroutine(coEnergyStagger());
                }
                else if (value > maxEnergy)
                {
                    Energy1 = maxEnergy;
                }
                else
                {
                    Energy1 = value;
                }
            }
            else
            {
                if (value < 0)
                {
                    Energy2 = 0;
                    StartCoroutine(coEnergyStagger());
                }
                else if (value > maxEnergy)
                {
                    Energy2 = maxEnergy;
                }
                else
                {
                    Energy2 = value;
                }
            }
        }
    }

    public Image chargeBar;

    private Rigidbody2D rb;
    private Rigidbody2D otherRb;

    LivesManager manager;

    // Use this for initialization
    void Start ()
    {
        // set a few initial variables
        minDashingSpeed = moveSpeed * 1.3f;
        otherRb = otherPlayer.GetComponent<Rigidbody2D>();

        rb = gameObject.GetComponent<Rigidbody2D>();
        
        manager = GetComponent<LivesManager>();
    }

        
    void Update ()
    {
        EnergyBarUI();

        // only run the controls if the game is running, the player is not stunned, and the game is active in hierarchy
        if(!staggered && Time.timeScale == 1 && this.gameObject.activeInHierarchy)
        {
            Controlls();
        }

        // speed limit code
        if (rb.velocity.magnitude > maxSpeed)
        {
            rb.velocity = rb.velocity.normalized * maxSpeed;
        }

        // speed checker for debugging
        myCurrentSpeed = Vector3.Distance(transform.position, lastPos) / Time.deltaTime;
        lastPos = transform.position;

        // set the characters sprite color based on a variety of conditions
        if(invincible == true)
        {
            GetComponent<SpriteRenderer>().color = Color.yellow;
        }
        else if(invincible == false && staggered == false)
        {
            GetComponent<SpriteRenderer>().color = Color.white;
        }

        if(staggered == true)
        {
            GetComponent<SpriteRenderer>().color = Color.grey;

        } 

    }

    void FixedUpdate()
    {
        // gives the player 100 energy per second
        energy++;
    }


    public void Controlls()
    {
        //place the horizontal and veritcal axis in a variable
        float x = Input.GetAxis(horizontalAxis);
        float y = Input.GetAxis(verticalAxis);

        if (!isCharging)
        {
            //transforms the game object in the y or x axis
            transform.Translate(x * Time.deltaTime * moveSpeed, y * Time.deltaTime * moveSpeed, 0, Space.World);
        }
        else
        {
            transform.Translate(x * Time.deltaTime * chargingMoveSpeed, y * Time.deltaTime * chargingMoveSpeed, 0, Space.World);
        }
        //place the horizontal and veritcal axis in a variable
        float rx = Input.GetAxis(horizontalRight);
        float ry = Input.GetAxis(verticalRight);

        //gets an angle based on the x and y axis and gets the Tan value, then rotates the object based on Euler angles
        if(Mathf.Abs( rx )>deadZone || Mathf.Abs( ry) > deadZone)
        {

            float angle = Mathf.Atan2(ry, rx) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));
        }

        //code gotten from http://answers.unity3d.com/questions/432048/how-to-rotate-object-to-location-of-joystick.html


        //if the trigger is different than 0 then the player is charging a dash
        if (Input.GetAxis(triggerRight) != 0)
        {
            if (isCharging == false)
            {
                isCharging = true;
            }

            //adds charge power up to 100
            if (chargePower < 100)
            {
                chargePower++;
            }
            else if (chargePower == 100)
            {
                // ADD FEEDBACK FOR WHEN PLAYER IS AT FULL DASH CHARGE !!!
            }

            energy--;
        }


        //if the trigger is 0 then the player dashes
        if (Input.GetAxis(triggerRight) == 0 && !isDashing)
        {
            Rigidbody2D Square = GetComponent<Rigidbody2D>();
            
            isCharging = false;
            
            if (chargePower < 20 && chargePower != 0)
            {
                // small dash
                Square.AddForce(transform.up * weakDashPower);
                StartCoroutine(coResetDashing());

                energy = energy - quickDashCost;
                Dash.Play();
            }
            if (chargePower >= 20)
            {
                // big dash
                Square.AddForce(transform.up * chargePower * strongDashPower / 25);
                StartCoroutine(coResetDashing());

                energy = energy - chargeDashCost;
                Dash.Play();
            }
            chargePower = 0;
        }
    }
    

    
    private void OnCollisionEnter2D(Collision2D other)
    {
        // check conditions on collision to decide course of action
        if (other.gameObject == otherPlayer.gameObject && isDashing)
        {
            StartCoroutine(coRoutineStagger());
        }
        if (other.gameObject.tag == otherPlayer.tag && rb.velocity.magnitude > minDashingSpeed || other.gameObject.tag == "Bouncy" && rb.velocity.magnitude > minDashingSpeed)
        {
            playerHit.Play();
        }

        if (other.gameObject.CompareTag("KillerWall") || other.gameObject.CompareTag("Missile"))
       {
            JustDied();
       }
    }

    private void JustDied()
    {
        levelManager.instance.PlayerRespawn(this);
    }

    private void EnergyBarUI()
    {
        chargeBar.fillAmount = (float)energy / (float)maxEnergy;
    }

    IEnumerator coRoutineStagger()
    {
        otherPlayer.staggered = true;

        yield return new WaitForSeconds(1f);

        staggered = false;

        otherPlayer.staggered = false;
    }

    IEnumerator coResetDashing()
    {
        print("reset dashing");
        isDashing = true;
        yield return new WaitForSeconds(0.5f);
        while (myCurrentSpeed > minDashingSpeed)
            yield return null; //waits for 1 frame

        isDashing = false;
    }

    IEnumerator coEnergyStagger()
    {
        // called when player is stunned due to lack of energy
        
        PowerDown.Play();

        staggered = true;
        print("Energy Stun Initiated");

        while (energy < maxEnergy)
        {
            energy++;
            yield return new WaitForSeconds(0.1f);
        }

        staggered = false;
        print("Energy Stun Ended");

        GetComponent<SpriteRenderer>().color = Color.white;

        PowerUp.Play();

        yield return null;
    }

}
