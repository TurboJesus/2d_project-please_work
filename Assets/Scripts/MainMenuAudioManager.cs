﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuAudioManager : MonoBehaviour {
    
    public AudioSource intro, loop;
    private bool startedLoop = false;
    
	void Start ()
    {
        startedLoop = false;
        intro.Play();
	}
	
	void FixedUpdate () {

        // once the intro is done, start the loop
        if (!intro.isPlaying && !startedLoop)
        {
            loop.Play();

            startedLoop = true;
        }
    }
}
