﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

    public GameObject transport;
    public AudioSource teleportAudio;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        // teleport respective player to respective position
        if(other.gameObject.tag == "Player 1" || other.gameObject.tag == "Player 2")
        {
            other.gameObject.transform.position = transport.transform.position;
            teleportAudio.Play();
        }
    }
}
