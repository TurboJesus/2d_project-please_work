﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathWall : MonoBehaviour {


    public LivesManager lifesystem;
    Animator anim;
    public levelManager respawn;

    public AudioSource die;

    PlayerInput pl;

    // Use this for initialization
    void Start() {
        // Set initial values
        lifesystem = FindObjectOfType<LivesManager>();
        anim = GetComponent<Animator>();
        respawn = FindObjectOfType<levelManager>();

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // if the object colliding with it has a playerscript, set it as "pl"
        pl = other.gameObject.GetComponent<PlayerInput>();

        // if it has missile as a tag
        if (other.tag == "Missile")
        {

            other.gameObject.SetActive(false);

        }
        
        // as long as the player isn't invincible
        if (pl.invincible == false) { die.Play();
            print("Lord help me");
        }
    }
}
